Env = {
	DeviceID = "wigmake"
}

WIGInternal = {}

WIGInternal.LogMessage = function (a, b) end
WIGInternal.MessageBox = function (a, b, c, d, e) end
WIGInternal.GetInput = function (a) end
WIGInternal.NotifyOS = function (a) end
WIGInternal.ShowScreen = function (a, b) end
WIGInternal.ShowStatusText = function (a) end

WIGInternal.AttributeChangedEvent = function (a, b) end
WIGInternal.CartridgeEvent = function (a) end
WIGInternal.CommandChangedEvent = function (a) end
WIGInternal.InventoryEvent = function (a, b, c) end
WIGInternal.MediaEvent = function (a, b) end
WIGInternal.TimerEvent = function (a, b) end
WIGInternal.ZoneStateChangedEvent = function (a) end

WIGInternal.IsPointInZone = function (a, b) end
WIGInternal.VectorToZone = function (a, b) end
WIGInternal.VectorToSegment = function (a, b, c) end
WIGInternal.VectorToPoint = function (a, b) end
WIGInternal.TranslatePoint = function (a, b, c) end
