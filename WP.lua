-- Wherigo helper functions


ZonePoint = Wherigo.ZonePoint
Player = Wherigo.Player
Distance = Wherigo.Distance

WP = {
	PK = nil,

	CARTRIDGE_DEFAULTS = {
		Activity = "Puzzle",
		Company = "",
		TargetDevice = "SmartPhone",
		TargetDeviceVersion = "0",
		BuilderVersion = "Wigmake",
		StateId = "1",
		CountryId = "2",
		Description = [[]],
		StartingLocationDescription = [[]],
		Visible = true,
		Complete = false,
		LastPlayedDate = "1/1/1970 01:00:00 AM"
	},

	ITEM_DEFAULTS = {
		Visible = true,
		Locked = false,
		Opened = false,
		ObjectLocation = Wherigo.INVALID_ZONEPOINT
	},
	
	INPUT_DEFAULTS = {
		InputType = "Text",
		Visible = true
	},

	COMMAND_DEFAULTS = {
		CmdWith = false,
		Enabled = true,
        Custom = true
	},
	
	ZONE_DEFAULTS = {
		DistanceRangeUOM = "Meters",
		ProximityRangeUOM = "Meters",
		Active = false,
		Visible = false,
		ShowObjects = "OnEnter",
		AllowSetPositionTo = false,
		ProximityRange = Wherigo.Distance(0, "meters")
	},
	
	TASK_DEFAULTS = {
		CorrectState = "None",
		Active = false,
		Visible = true,
		Complete = false
	},

	GUID = nil,
	Cartridge = nil,
	mediamap = nil
}


-- WP static member constructor
-- Create a cartridge
--
function WP.init()
	WP.GUID = 0
	WP.Cartridge = Wherigo.ZCartridge()
	WP.setDefaults(WP.Cartridge, WP.CARTRIDGE_DEFAULTS)
	WP.Cartridge.Id = WP.guid()
	WP.mediamap = {}
end


-- Unique ID generator
--
function WP.guid()
	WP.GUID = WP.GUID + 1
	return tostring(WP.GUID)
end


-- Set default object properties
--
-- @param obj		object instance
-- @param defaults	defaults table
--
function WP.setDefaults(obj, defaults) 
	for key, value in pairs(defaults) do
		obj[key] = value
	end
end


-- Reads MediaFiles from object and load it's media to Media table
--
-- @param obj  object with MediaFiles table
--
function WP.loadMedia(obj)
	obj.Media = {}
	for _, v in ipairs(obj.MediaFiles) do
		obj.Media[v[1]] = WP.ZMedia(v[2])
	end
end


-- Item creation helper
--
-- @param  zone or player containing the item
--
function WP.ZItem(container)
	local item = Wherigo.ZItem({ Cartridge=WP.Cartridge, Container=container })
	WP.setDefaults(item, WP.ITEM_DEFAULTS)
	item.Id = WP.guid()
	item.ObjectLocation = nil
	return item
end


-- Input creation helper
--
function WP.ZInput()
	local input = Wherigo.ZInput(WP.Cartridge)
	WP.setDefaults(input, WP.INPUT_DEFAULTS)
	input.Id = WP.guid()
	input.Name = input.Id
	input.InputVariableId = WP.guid()
	return input
end


-- Command creation helper
--
-- @param text  	command button text
-- @param enabled 	if False, the command disabled
--
function WP.ZCommand(text, enabled)
	if enabled == nil then
		enabled = WP.COMMAND_DEFAULTS.Enabled
	end
	local cmd = Wherigo.ZCommand({Id=WP.guid(), Text=text, CmdWith=WP.COMMAND_DEFAULTS.CmdWith, Enabled=enabled})
	cmd.Custom = WP.COMMAND_DEFAULTS.Custom
	return cmd
end


-- Zone creation helper
--
function WP.Zone()
	local zone = Wherigo.Zone(WP.Cartridge)
	WP.setDefaults(zone, WP.ZONE_DEFAULTS)
	zone.Id = WP.guid()
	return zone
end


-- Timer creation helper
--
function WP.ZTimer()
	local timer = Wherigo.ZTimer(WP.Cartridge)
	timer.Id = WP.guid()
	timer.Type = "Interval"
	timer.Visible = false
	return timer
end


-- Task creation helper
--
function WP.ZTask(name)
	local task = Wherigo.ZTask(WP.Cartridge)
	WP.setDefaults(task, WP.TASK_DEFAULTS)
	task.Id = WP.guid()
	return task
end


-- Media creation helper
--
-- @param file  media file name
--
function WP.ZMedia(file)
	if file == nil then
		return nil
	end

	dotPos = file:find(".", 1, true)
	assert(dotPos ~= nil)

	media = Wherigo.ZMedia(WP.Cartridge)
	media.Id = WP.guid()
	media.Name = media.Id
	media.AltText = ""
	media.Resources = { 
		{ Type = file:sub(dotPos + 1), Filename = file, Directives = {} }
	}
	table.insert(WP.mediamap, media)
	return media
end


-- Decode an obfuscated string
--
-- @param str  obfuscated string
--
function WP.dec(str)
	assert(WP.PK ~= nil)

	local out = ""
	local j = 0
	for i = 1, #str do
		local c = string.byte(str, i)
		local d
		if c >= 32 and c <= 126 then
			local e = string.byte(WP.PK, j % #WP.PK + 1)
			d = (c - 32 - e + 95*2) % 95 + 32
			j = j + 1
		else
			d = c
		end
		out = out..string.char(d)
	end
	return out
end


-- Deactivate all objects, effectively prevent playing the cartridge
--
function WP.abort()
	for _, obj in pairs(WP.Cartridge.AllZObjects) do
		obj.Active = false
		obj.Visible = false
	end
end
