import sys, os, re, subprocess, lupa
import media
from datetime import datetime
from struct import pack


class Compiler:

    def __init__(self, mainfile, libdirs=[], config=[], testMode=False, obfKey=None, luacPath=None, deviceId=None, playerName="", verbose=False):
        self.mainfile = mainfile
        self.maindir = os.path.dirname(self.mainfile)
        self.sysdir = os.path.dirname(os.path.realpath(__file__))
        self.tmpdir = "/tmp/wigmake"
        try:
            os.mkdir(self.tmpdir)
        except FileExistsError:
            pass
        self.packagePath = 'package.path=package.path..";' + os.path.join(self.sysdir, "?.lua") + '"'
        self.luacPath = luacPath or "luac"
        self.deviceId = deviceId 
        self.testMode = testMode
        self.playerName = playerName
        self.verbose = verbose

        if obfKey:
            self.obfuscate(obfKey)
        
        self.initCode = 'require("Wherigo")\n'
        self.initCode += "Config = {\n"
        self.initCode += "  TestMode=" + ("true" if self.testMode else "false") + ",\n"
        if self.deviceId:
            self.initCode += '  DeviceID="%s",\n' % self.deviceId
        if config:
            for cfg in config:
                self.initCode += "  " + cfg + ",\n"
        self.initCode += "}\n"

        self.lua = lupa.LuaRuntime()
        self.lua.execute(self.packagePath)
        self.execLua(self.sysdir, "WIGInternal.lua")
        self.lua.execute(self.initCode)
        
        self.sources = [os.path.join(self.sysdir, "WP.lua")]
        self.execLua(self.sysdir, "WP.lua")
        for libdir in libdirs:
            for f in os.listdir(libdir):
                if f.endswith(".lua") and os.path.realpath(os.path.join(libdir, f)) != os.path.realpath(mainfile):
                    self.sources.append(os.path.join(libdir, f))
                    self.execLua(libdir, f)

        self.sources.append(self.mainfile)
        self.cartridge = self.execLua("", self.mainfile)
        if not self.cartridge:
            raise Exception("The main file %s must return an object of type Cartridge" % mainfile)
        
        self.mediaSet = media.MediaSet(self.maindir, self.cartridge, self.deviceId)
        if self.verbose:
            self.mediaSet.print()


    def execLua(self, dir, filename):
        with open(os.path.join(dir, filename), "r") as f:
            try:
                return self.lua.execute(f.read())
            except lupa._lupa.LuaError as e:
                raise Exception("Lua error in %s: %s" % (filename, str(e)))


    def compile(self):
        if self.verbose:
            print("Compiling Lua sources:")
        cartfile = os.path.join(self.tmpdir, "cartridge.lua")
        cart = open(cartfile, "w")
        cart.write(self.initCode)
        for src in self.sources:
            if self.verbose:
                print("  " + src)
            f = open(src, "r")
            cart.write(f.read())
            cart.write("\n")
            f.close()
        cart.close()
        self.code = subprocess.check_output([self.luacPath] + ([] if self.testMode else ["-s"]) + ["-o", "-", cartfile])


    def obfuscate(self, obfKey):
        source = open(self.mainfile, "r")
        self.mainfile = os.path.join(self.tmpdir, os.path.basename(self.mainfile))
        target = open(self.mainfile, "w")
        data = source.read()
        target.write('WP.PK = "%s"\n' % obfKey)

        i = 0
        while i < len(data):
            if i < len(data)-3 and data[i] == "P" and data[i+1] == "(" and data[i+2] == '"':
                target.write('WP.dec("')
                i += 3
                j = 0
                while data[i] != '"':
                    c = ord(data[i])
                    if c >= 32  and c <= 126:
                        e = ord(obfKey[j % len(obfKey)]) 
                        d = (c - 32 + e) % 95 + 32
                        if d == ord('"') or d == ord('\\') or d == ord('&') or d == ord('<') or d == ord('>'):
                            target.write("\\%03d" % d)
                        else:
                            target.write(chr(d))
                        j += 1
                    else:
                        target.write(chr(c))
                    i += 1
                target.write('"')
                i += 1
            else:
                target.write(data[i])
                i += 1
        source.close()
        target.close()

    def loadBuildNr(self):
        try:
            with open(os.path.join(self.maindir, "_buildnr"), "r") as f:
                self.buildnr = int(f.read()) + 1
        except FileNotFoundError:
            self.buildnr = 1

    def storeBuildNr(self):
        with open(os.path.join(self.maindir, "_buildnr"), "w") as f:
            f.write(str(self.buildnr))


    def writeStr(self, output, s):
        if s:
            output.extend(s.encode())
        output.append(0)

    def write(self):
        if not self.cartridge.StartingLocation:
            raise Exception("No cartridge starting location defined")
        if not self.cartridge.Id:
            raise Exception("No cartridge ID defined")
        
        output = bytearray(b"\x02\x0aCART\x00")
        self.mediaSet.writeMediaTable(output)

        start = self.cartridge.StartingLocation
        creationDate = (datetime.now() - datetime(2004, 2, 10, 1, 0, 0)).total_seconds()
        output.extend(pack("<idddq", 0, start.latitude, start.longitude, 0, int(creationDate)))
        output.extend(pack("<h", self.cartridge.Poster._id if self.cartridge.Poster else -1))
        output.extend(pack("<h", self.cartridge.Icon._id if self.cartridge.Icon else -1))
        self.writeStr(output, self.cartridge.Activity)
        self.writeStr(output, self.playerName)
        output.extend(bytearray(8))
        
        self.writeStr(output, self.cartridge.Name)
        self.writeStr(output, self.cartridge.Id)
#        output.extend(pack("<q", int(self.cartridge.Id.replace("-", ""), 16)))
        self.writeStr(output, self.cartridge.Description)
        self.writeStr(output, self.cartridge.StartingLocationDescription)

        version = self.cartridge.Version if self.cartridge.Version else "SNAPSHOT"
        self.loadBuildNr()
        version = version + "-" + str(self.buildnr)
        self.writeStr(output, version)
        print("Version: " + version)
        self.storeBuildNr()

        self.writeStr(output, self.cartridge.Author)
        self.writeStr(output, self.cartridge.Company)
        self.writeStr(output, self.cartridge.TargetDevice)
        
        output.extend(pack("<i", 15))
        output.extend(bytearray(15))
        
        pos = 9 + 6 * (self.mediaSet.size() + 1)
        output[pos:pos+4] = pack("<i", len(output) - pos - 4)

        output[11:15] = pack("<i", len(output))
        output.extend(pack("<i", len(self.code)))
        output.extend(self.code)

        self.mediaSet.write(output)
        return output

