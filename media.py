import os
from enum import Enum
from struct import pack

MediaTypes = { "bmp": 1, "png": 2, "jpg": 3, "gif": 4, "wav": 17, "mp3": 18, "fdl": 19, "snd": 20, "ogg": 21, "swf": 33, "txt": 49 }


class Media:
    def __init__(self, dir, mediaObj):
        ress = mediaObj.Resources
        if len(ress) != 1:
            raise Exception("Invalid number of media resources: %i" % len(ress))
        res = ress[1]
        self.filename = os.path.join(dir, res.Filename)
        try:
            self.type = MediaTypes[res.Type]
            with open(self.filename, "rb") as f:
                self.data = f.read()
        except KeyError:
            raise Exception("Unknown media type: %s" % res.Type)
        except FileNotFoundError:
            raise Exception("Media file not found: %s" % self.filename)

    def write(self, out):
        out.extend(pack("<bii", 1, self.type, len(self.data)))
        out.extend(self.data)


class MediaSet:
    def __init__(self, dir, cartridge, deviceId):
        self.media = {}
        self.deviceId = deviceId
        if not "AllZObjects" in cartridge:
            raise Exception("Invalid cartridge, no AllZObjects defined")
        for obj in cartridge.AllZObjects.values():
            if obj._classname == "ZMedia":
                self.media[obj.ObjIndex] = Media(dir, obj)

    def size(self):
        return len(self.media)

    def writeMediaTable(self, out):
        out.extend(pack("<H", len(self.media) + 1))
        out.extend(bytearray(6 * (len(self.media) + 1)))

    def write(self, out):
        i = 1
        for objId, media in self.media.items():
            pos = 9 + i * 6
            if self.deviceId == "webwigo":
                id = objId
            else:
                id = i
            out[pos:pos+6] = pack("<Hi", id, len(out))
            media.write(out) 
            i += 1

    def print(self):
        print("Media:")
        for id, media in self.media.items():
            print("  %i: %s" % (id, media.filename))
