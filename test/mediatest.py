import testbase, media
from struct import unpack

class MediaTest(testbase.TestCase):

    def testMedia(self):
        cartridge = self.compile("lua/testMedia/cartridge.lua")
        mediaSet = media.MediaSet("lua/testMedia", self.cartridge, "")
        ms = mediaSet.media
        self.assertEqual(len(ms), 2)
        self.assertEqual(ms[1].type, 2)
        self.assertEqual(ms[1].data, b"\x89\x50\x4e\x47\x0d\x0a\x1a\x0a\x00\x00\x00\x0d\x49\x48\x44\x52\x00\x00\x00\x01\x00\x00\x00\x01\x08\x02\x00\x00\x00\x90\x77\x53\xde\x00\x00\x00\x0f\x49\x44\x41\x54\x08\x1d\x01\x04\x00\xfb\xff\x00\xff\xff\xff\x05\xfe\x02\xfe\x03\x7d\x19\xc6\x00\x00\x00\x00\x49\x45\x4e\x44\xae\x42\x60\x82")
        self.assertEqual(ms[2].type, 49)
        self.assertEqual(ms[2].data, b"Ahoj\n")

    def testInvalidFile(self):
        cartridge = self.compile("lua/testMedia/cartridge2.lua")
        try:
            mediaSet = media.MediaSet("lua/testMedia", self.cartridge, "")
            self.fail("Should throw Exception")
        except Exception:
            pass

    def testWrite(self):
        cartridge = self.compile("lua/testMedia/cartridge.lua")
        mediaSet = media.MediaSet("lua/testMedia", self.cartridge, "")
        out = bytearray(7)
        mediaSet.writeMediaTable(out)
        mediaSet.write(out)
        
        self.assertEqual(unpack("<H", out[7:9])[0], 3)
        self.assertEqual(unpack("<H", out[9:11])[0], 0)
        self.assertEqual(unpack("<H", out[15:17])[0], 1)
        self.assertEqual(unpack("<H", out[21:23])[0], 2)

        pos = unpack("<i", out[17:21])[0]
        self.assertEqual(unpack("<bii", out[pos:pos+9]), (1, 2, 72))
        self.assertEqual(out[pos+9:pos+81], b"\x89\x50\x4e\x47\x0d\x0a\x1a\x0a\x00\x00\x00\x0d\x49\x48\x44\x52\x00\x00\x00\x01\x00\x00\x00\x01\x08\x02\x00\x00\x00\x90\x77\x53\xde\x00\x00\x00\x0f\x49\x44\x41\x54\x08\x1d\x01\x04\x00\xfb\xff\x00\xff\xff\xff\x05\xfe\x02\xfe\x03\x7d\x19\xc6\x00\x00\x00\x00\x49\x45\x4e\x44\xae\x42\x60\x82")

        pos = unpack("<i", out[23:27])[0]
        self.assertEqual(unpack("<bii", out[pos:pos+9]), (1, 49, 5))
        self.assertEqual(out[pos+9:pos+16], b"Ahoj\n")
