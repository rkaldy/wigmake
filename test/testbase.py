import unittest, lupa
import os, sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)


class TestCase(unittest.TestCase):

    def setUp(self):
        self.lua = lupa.LuaRuntime()
        self.lua.execute('package.path=package.path..";../?.lua"')

    def compile(self, luaSrc):
        with open("../WIGInternal.lua", "rb") as f:
            self.lua.execute(f.read())
        with open(luaSrc) as f:
            self.cartridge = self.lua.execute(f.read())
