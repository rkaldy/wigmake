require "Wherigo"

cart = Wherigo.ZCartridge()

png = Wherigo.ZMedia(cart)
png.Resources = {{ Type="png", Filename="media.png" }}

txt = Wherigo.ZMedia(cart)
txt.Resources = {{ Type="txt", Filename="invalid.txt" }}

return cart
