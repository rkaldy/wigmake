import os, subprocess, testbase, argparse
from struct import unpack
from compiler import Compiler

class CompilerTest(testbase.TestCase):

    def testCompile(self):
        comp = Compiler("lua/init.lua")
        self.assertEqual(len(comp.cartridge.AllZObjects), 2)
        self.assertEqual(comp.cartridge.item.func(), 1896)
        self.assertEqual(comp.lua.globals().Config.TestMode, False)

    def testLibraries(self):
        comp = Compiler("lua/lib.lua", libdirs=["lua/testLib1", "lua/testLib2"])
        self.assertEqual(comp.cartridge.str, "Hello world")

    def testObfuscate(self):
        comp = Compiler("lua/obfuscate.lua", obfKey="obfkey")
        self.assertEqual(comp.lua.globals().WP.PK, "obfkey")
        self.assertEqual(comp.cartridge.str, "příliš žluťoučký kůň")

    def testTestMode(self):
        comp = Compiler("lua/init.lua", testMode=True)
        self.assertEqual(comp.lua.globals().Config.TestMode, True)

    def testBuildNr(self):
        try:
            os.remove("lua/_buildnr")
        except FileNotFoundError:
            pass
        comp = Compiler("lua/init.lua")
        comp.loadBuildNr()
        self.assertEqual(comp.buildnr, 1)
        comp.storeBuildNr()
        comp.loadBuildNr()
        self.assertEqual(comp.buildnr, 2)
        os.remove("lua/_buildnr")

    def testWrite(self):
        comp = Compiler("lua/testWrite/main.lua")
        comp.compile()
        gwc = comp.write()

        self.assertEqual(gwc[9+6*2+50:9+6*2+55], b"Test\0")
        
        self.assertEqual(unpack("<h", gwc[9:11])[0], 0)
        pos = unpack("<i", gwc[11:15])[0]
        ln = unpack("<i", gwc[pos:pos+4])[0]
        code = gwc[pos+4:pos+4+ln]
        with open("cartridge.luac", "wb") as f:
            f.write(comp.code)
        output = subprocess.check_output(["lua", "-e", comp.packagePath + '; require("WIGInternal")', "cartridge.luac"])
        self.assertEqual(output, b"ZMedia\n")
        os.remove("cartridge.luac")
        
        self.assertEqual(unpack("<h", gwc[15:17])[0], 1)
        pos = unpack("<i", gwc[17:21])[0]
        self.assertEqual(gwc[pos], 1)
        self.assertEqual(unpack("<i", gwc[pos+1:pos+5])[0], 2)
        ln = unpack("<i", gwc[pos+5:pos+9])[0]
        self.assertEqual(gwc[pos+9:pos+9+ln], b"\x89\x50\x4e\x47\x0d\x0a\x1a\x0a\x00\x00\x00\x0d\x49\x48\x44\x52\x00\x00\x00\x01\x00\x00\x00\x01\x08\x02\x00\x00\x00\x90\x77\x53\xde\x00\x00\x00\x0f\x49\x44\x41\x54\x08\x1d\x01\x04\x00\xfb\xff\x00\xff\xff\xff\x05\xfe\x02\xfe\x03\x7d\x19\xc6\x00\x00\x00\x00\x49\x45\x4e\x44\xae\x42\x60\x82")
        os.remove("lua/testWrite/_buildnr")
